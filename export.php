<?php
require('pdo.php');
$pdo->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, false);
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


function addPetitionCampaigns($fp) {
    global $separator;
    global $importUser;
    global $robinUser;

    $petitions = [
        [1181, 'Hühnerleid bei Wiesenhof',                       null, null, null, 'In Progress', "TRUE", null, $importUser, $robinUser, 'Petition', 'Petition'],
        [1206, 'Hunde- und Katzenhandel in China',               null, null, null, 'In Progress', "TRUE", null, $importUser, $robinUser, 'Petition', 'Petition'],
        [1233, 'Rewe',                                           null, null, null, 'In Progress', "TRUE", null, $importUser, $robinUser, 'Petition', 'Petition'],
        [1276, 'No Animal Left Behind',                          null, null, null, 'In Progress', "TRUE", null, $importUser, $robinUser, 'Petition', 'Petition'],
        [1286, 'Pantanal',                                       null, null, null, 'In Progress', "TRUE", null, $importUser, $robinUser, 'Petition', 'Petition'],
        [1353, 'Foie Gras',                                      null, null, null, 'In Progress', "TRUE", null, $importUser, $robinUser, 'Petition', 'Petition'],
        [1354, 'EU For Animals',                                 null, null, null, 'In Progress', "TRUE", null, $importUser, $robinUser, 'Petition', 'Petition'],
        [950,  'Gadhimai',                                       null, null, null, 'In Progress', "TRUE", null, $importUser, $robinUser, 'Petition', 'Petition'],
        [821,  'Grausame Spektakel: die Stierfeste von Spanien', null, null, null, 'In Progress', "TRUE", null, $importUser, $robinUser, 'Petition', 'Petition'],
        [820,  'Hähnchenmast: 42 Tage Hölle',                    null, null, null, 'In Progress', "TRUE", null, $importUser, $robinUser, 'Petition', 'Petition'],
        [822,  'Hennenqual für\'s Horror-Ei',                    null, null, null, 'In Progress', "TRUE", null, $importUser, $robinUser, 'Petition', 'Petition'],
        [819,  'Küken Finn',                                     null, null, null, 'In Progress', "TRUE", null, $importUser, $robinUser, 'Petition', 'Petition'],
        [1098, 'Lebendtiermärkte verbieten',                     null, null, null, 'In Progress', "TRUE", null, $importUser, $robinUser, 'Petition', 'Petition'],
        [818,  'Schweinhölle in Spanien',                        null, null, null, 'In Progress', "TRUE", null, $importUser, $robinUser, 'Petition', 'Petition'],
        [1044, 'Kastenstand (Change.org)',                       null, null, null, 'In Progress', "TRUE", null, $importUser, $robinUser, 'Petition', 'Petition'],
        [null, 'Matching Gift - Ende 2021',                      null, null, null, 'Completed',   "FALSE", null, $importUser, $robinUser, 'Fundraising', 'Matching Gift'],
        [null, 'Weihnachtspost',                                 null, null, null, 'In Progress', "TRUE", null, $importUser, $robinUser, 'Marketing', 'Direct Mail'],
        [27,   'Weihnachtspost 2016',                            null, null, null, 'Completed',   "FALSE", null, $importUser, $robinUser, 'Marketing', 'Direct Mail', 'Child von Weihnachtspost'],
        [48,   'Weihnachtspost 2017',                            null, null, null, 'Completed',   "FALSE", null, $importUser, $robinUser, 'Marketing', 'Direct Mail', 'Child von Weihnachtspost'],
        [59,   'Weihnachtspost 2018',                            null, null, null, 'Completed',   "FALSE", null, $importUser, $robinUser, 'Marketing', 'Direct Mail', 'Child von Weihnachtspost'],
        [60,   'Weihnachtspost 2019',                            null, null, null, 'Completed',   "FALSE", null, $importUser, $robinUser, 'Marketing', 'Direct Mail', 'Child von Weihnachtspost'],
        [null, 'Wirkungsbericht',                                null, null, null, 'In Progress', "TRUE", null, $importUser, $robinUser, 'Marketing', 'Direct Mail'],
        [33,   'Wirkungsbericht 2015',                           null, null, null, 'Completed',   "FALSE", null, $importUser, $robinUser, 'Marketing', 'Direct Mail', 'Child von Wirkungsbericht'],
        [39,   'Wirkungsbericht 2016',                           null, null, null, 'Completed',   "FALSE", null, $importUser, $robinUser, 'Marketing', 'Direct Mail', 'Child von Wirkungsbericht'],
        [1320, 'Matching Gift - 2021 Donors & NL',               'EOY MGC 2021 donors & NL', null, null, 'Completed', "FALSE", null, $importUser, $robinUser, 'Fundraising', 'Matching Gift'],
        [1323, 'One Time Donors 2021 & NL',                      null, null, null, 'Completed',   "FALSE", null, $importUser, $robinUser, 'Fundraising', 'Other'],
        [1360, 'Non Monthly Members',                            'Mitglieder und Dauerspender, die als Zahlungsturnus vierteljährlich, halbjährlich oder jährlich ausgewählt haben', null, null, 'Completed',   "FALSE", null, $importUser, $robinUser, 'General', 'Other'],
        [1363, 'Organization',                                   null, null, null, 'Completed',   "FALSE", null, $importUser, $robinUser, 'General', 'Other'],
        [1364, 'Geschenk-Mitgliedschaftskontakte',               null, null, null, 'Completed',   "FALSE", null, $importUser, $robinUser, 'General', 'Other'],
        [1365, 'Nonprofit Org',                                  null, null, null, 'Completed',   "FALSE", null, $importUser, $robinUser, 'General', 'Other'],
        [1366, 'Foundation Org',                                 null, null, null, 'Completed',   "FALSE", null, $importUser, $robinUser, 'General', 'Other'],
        [1367, 'Government Org',                                 null, null, null, 'Completed',   "FALSE", null, $importUser, $robinUser, 'General', 'Other'],
        [1368, 'Corporate Org',                                  null, null, null, 'Completed',   "FALSE", null, $importUser, $robinUser, 'General', 'Other'],
        [947,  'Mitglieder ohne eigene Zahlung',                 null, null, null, 'Completed',   "FALSE", null, $importUser, $robinUser, 'Fundraising', 'Other'],
    ];

    foreach ($petitions as $petition) {
        fputcsv($fp, array_values($petition), $separator);
    }
}

function specificContacts($idField, $glue = '') {
    $ids = [12846, 13927, 18651, 26350, 27416, 33399, 38359, 49158, 51472, 53139, 57235, 61511, 64572, 78306, 84421, 99448, 128846, 129028, 132800, 136294, 139562, 179695, 189292, 201887, 213413, 220989, 221132, 221333, 221488, 221498, 221567, 221567, 221568, 221573, 221574, 221587, 221675, 221679, 221681, 221691, ];

    if (empty($ids)) {
        return ' ';
    }

    return ' ' . $glue . ' ' . $idField . ' IN (' . implode(',', $ids) . ') ';
}

function fromDate($dateField, $glue = '') {
    $date = '2022-05-04 10:40:00';

    if (empty($date)) {
        return ' ';
    }

    return ' ' . $glue . ' ' . $dateField . ' > "' . $date . '"';
}

function alterRow($fileName, $row) {
    if ($fileName === 'contact') {
        if ($row['email_aktiv']) {
            $row['email_alternate'] = $row['email_aktiv'];
        } else {
            $row['email_alternate'] = $row['email_other'];
        }
    } else if ($fileName === 'lead') {
        if ($row['email_home']) {
            $row['email_alternate'] = $row['email_home'];
        } else if ($row['email_aktiv']) {
            $row['email_alternate'] = $row['email_aktiv'];
        } else if ($row['email_other']) {
            $row['email_alternate'] = $row['email_other'];
        } else if ($row['email_work']) {
            $row['email_alternate'] = $row['email_work'];
        }
    } else if ($fileName === 'organization') {
        if ($row['phone_work']) {
            $row['phone_alternate'] = $row['phone_work'];
        } else if ($row['phone_home']) {
            $row['phone_alternate'] = $row['phone_home'];
        } else if ($row['phone_mobile']) {
            $row['phone_alternate'] = $row['phone_mobile'];
        } else if ($row['phone_other']) {
            $row['phone_alternate'] = $row['phone_other'];
        }

        if ($row['email_work']) {
            $row['email_alternate'] = $row['email_work'];
        } else if ($row['email_home']) {
            $row['email_alternate'] = $row['email_home'];
        }
    }

    return $row;
}

function exportCampaignMembers($type) {
    global $campaignIdsToExport;
    global $pdo;
    global $separator;
    global $contactDefiningCriteria;
    global $leadDefiningCriteria;
    global $importUser;

    $fp = fopen('exports/campaign_member_' . $type . '.csv', 'w');
    $firstRow = true;

    foreach ($campaignIdsToExport as $id) {
        $query = str_replace('{campaign_id}', $id, 'SELECT
            "{campaign_id}" as civi_campaign_id,
            c.id as civi_entity_id,
            "Confirmed" as status,
            "' . $importUser . '" as civi_sf_user,
            "campaign" as type
            FROM civicrm_contact as c {where}
            HAVING
                (SELECT count(*) FROM civicrm_membership as m WHERE m.contact_id = c.id AND m.campaign_id = {campaign_id} ' . fromDate('m.join_date', 'AND') . ') > 0 OR
                (SELECT count(*) FROM civicrm_contribution as con WHERE con.contact_id = c.id AND con.campaign_id = {campaign_id}' . fromDate('c.created_date', 'AND') . ') > 0 OR
                (SELECT count(*) FROM civicrm_contribution_recur as conr WHERE conr.contact_id = c.id AND conr.campaign_id = {campaign_id}' . fromDate('conr.create_date', 'AND') . ') > 0');

        if ($type === 'contact') {
            $stmt = $pdo->query(str_replace('{where}', 'WHERE c.contact_type = "Individual" ' . $contactDefiningCriteria . specificContacts('c.id', 'AND'), $query));
        } else if ($type === 'lead') {
            $stmt = $pdo->query(str_replace('{where}', 'WHERE c.contact_type = "Individual" ' . $leadDefiningCriteria . specificContacts('c.id', 'AND'), $query));
        } else if ($type === 'organization') {
            $stmt = $pdo->query(str_replace('{where}', 'WHERE c.contact_type = "Organization" ' . specificContacts('c.id', 'AND'), $query));
        }

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            if ($firstRow) {
                $headers = array_keys($row);
                fputcsv($fp, $headers, $separator);
                $firstRow = false;
            }

            fputcsv($fp, $row, $separator);
        }
    }

    fclose($fp);
}

$separator = ';';
$importUser = '00509000009mkaXAAQ';
$robinUser = '00509000007lyWxAAI';

$petitionGroupsIds = [1181, 1206, 1233, 1276, 1286, 1353, 1354, 950, 821, 820, 822, 819, 1098, 818, 1044];
$tagIdsToExport = [27, 48, 59, 60, 33, 39];
$campaignIdsToExport = [2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 26, 27, 28, 29, 30, 31, 32, 33, 44, 45];

$contactIdsContribution = 'SELECT c.id FROM civicrm_contact as c JOIN civicrm_contribution as cont ON c.id = cont.contact_id GROUP BY c.id';
$contactIdsInNewsletterOrPetitionGroup = 'SELECT gc.contact_id FROM civicrm_group_contact AS gc WHERE gc.status = "Added" AND group_id IN(676, 952, ' . implode(',', $petitionGroupsIds) . ')';
$contactIdsOrganization = 'SELECT c.id FROM civicrm_contact as c WHERE contact_type = "Organization"';

$contactDefiningCriteria = 'AND c.id IN (' . $contactIdsContribution . ')';
$leadDefiningCriteria = 'AND c.id NOT IN (' . $contactIdsContribution . ') AND c.id IN (' . $contactIdsInNewsletterOrPetitionGroup . ')';

$contactBaseQuery = 'SELECT
    c.id as civi_contact_id,
    c.contact_type,
    IF(c.do_not_email = 1, "TRUE", "FALSE") as do_not_email,
    IF(c.do_not_phone = 1, "TRUE", "FALSE") as do_not_phone,
    IF(c.do_not_mail = 1, "TRUE", "FALSE") as do_not_mail,
    "TRUE" as do_not_call,
    IF(c.preferred_language = "en_GB", "TRUE", "FALSE") as prefers_english,
    c.external_identifier,
    CASE
        WHEN c.source IS NOT NULL THEN c.source
        WHEN (SELECT count(*) FROM civicrm_entity_tag WHERE entity_table = "civicrm_contact" AND entity_id = c.id AND tag_id = 49) > 0 THEN "Twingle"
        WHEN (SELECT count(*) FROM civicrm_entity_tag WHERE entity_table = "civicrm_contact" AND entity_id = c.id AND tag_id = 50) > 0 THEN "Cygne/WP"
    END as source,
    c.first_name,
    c.last_name,
    c.formal_title,
    CASE
        WHEN c.gender_id IS NULL THEN ""
        WHEN c.gender_id = 1 THEN "Frau"
        WHEN c.gender_id = 2 THEN "Herr"
        WHEN c.gender_id = 3 THEN "Divers"
    END as salutation,
    c.birth_date,
    "' . $importUser . '" as civi_sf_user,
    "' . $robinUser . '" as owner_id,
    IF(c.is_deceased = 1, "TRUE", "FALSE") as is_deceased,
    c.deceased_date,
    c.organization_name,
    CASE
        WHEN (SELECT count(*) FROM civicrm_group_contact AS gc WHERE gc.contact_id = c.id AND gc.group_id = 1368 AND status = "Added") > 0 THEN "Corporate"
        WHEN (SELECT count(*) FROM civicrm_group_contact AS gc WHERE gc.contact_id = c.id AND gc.group_id = 1366 AND status = "Added") > 0 THEN "Foundation"
        WHEN (SELECT count(*) FROM civicrm_group_contact AS gc WHERE gc.contact_id = c.id AND gc.group_id = 1367 AND status = "Added") > 0 THEN "Government"
        WHEN (SELECT count(*) FROM civicrm_group_contact AS gc WHERE gc.contact_id = c.id AND gc.group_id = 1365 AND status = "Added") > 0 THEN "Nonprofit"
    END as account_type,
    c.employer_id,
    c.created_date,
    address.street_address,
    address.supplemental_address_1,
    address.city,
    address_sp.abbreviation as province_abbreviation,
    IF(LENGTH(address.postal_code) = 4 AND address_c.iso_code = "DE", CONCAT("0", address.postal_code), address.postal_code) as postal_code,
    address_c.iso_code as country_iso,
    address_l.name as location,
    k._ber_welche_kampagne_1,
    k._ber_welches_medium_2,
    k.gr_nde_motivation_f_r_ae_zu_spen_68,
    k.verfolgt_uns_ber_92,
    w.mitgliedsnummer_3,
    IF(w.spendenbescheinigung_4 = 1, "TRUE", IF((SELECT id FROM civicrm_group_contact WHERE status = "Added" AND group_id = 774 AND contact_id = c.id), "TRUE", "FALSE")) as spendenbescheinigung_4,
    IF(w.adresse_aktuell_56 = 0, "TRUE", "FALSE") as adresse_unzustellbar,
    IF(w.ehepaar_105 = 1, "TRUE", "FALSE") as ehepaar_105,
    m.mailchimp_source_114,
    m.function_mailchimp__115,
    m.optin_time_116,
    (SELECT IF(sh.status = "Added", sh.date, "") FROM civicrm_subscription_history AS sh WHERE sh.contact_id = c.id AND sh.group_id = 676 ORDER BY date DESC LIMIT 1) as hnl_confirm,
    a.aktiv_weitere_st_dte_156,
    a.optin_time_160,
    (SELECT IF(sh.status = "Added", sh.date, "") FROM civicrm_subscription_history AS sh WHERE sh.contact_id = c.id AND sh.group_id = 823 ORDER BY date DESC LIMIT 1) as fn_confirm,
    a.ad_optin_time_181,
    (SELECT IF(sh.status = "Added", sh.date, "") FROM civicrm_subscription_history AS sh WHERE sh.contact_id = c.id AND sh.group_id = 952 ORDER BY date DESC LIMIT 1) as ad_confirm,
    IF(d.iban_137 IS NOT NULL, d.iban_137, (SELECT iban FROM civicrm_contribution_recur AS r JOIN civicrm_sdd_mandate as m ON r.id = m.entity_id WHERE r.contribution_status_id = 2 AND r.contact_id = c.id LIMIT 1)) as iban_137,
    o.label as website_type,
    web.url as website_url,
    IF((SELECT count(*) FROM civicrm_group_contact AS gc WHERE gc.contact_id = c.id AND gc.group_id = 676 AND status = "Added"), "Yes", "") AS has_main_newsletter,
    IF((SELECT count(*) FROM civicrm_group_contact AS gc WHERE gc.contact_id = c.id AND gc.group_id = 952 AND status = "Added"), "Yes", "") AS has_ad_newsletter,
    (SELECT p.phone FROM civicrm_phone AS p WHERE p.contact_id = c.id AND p.location_type_id = 1 AND p.phone_type_id != 2 LIMIT 1) AS phone_home,
    (SELECT p.phone FROM civicrm_phone AS p WHERE p.contact_id = c.id AND p.phone_type_id = 2 LIMIT 1) AS phone_mobile,
    (SELECT p.phone FROM civicrm_phone AS p WHERE p.contact_id = c.id AND p.location_type_id = 2 LIMIT 1) AS phone_work,
    (SELECT p.phone FROM civicrm_phone AS p WHERE p.contact_id = c.id AND p.location_type_id = 4 LIMIT 1) AS phone_other,
    "" as phone_alternate,
    (SELECT IF(p.phone_type_id = 2, "Mobile", l.name) FROM civicrm_phone AS p LEFT JOIN civicrm_location_type AS l ON p.location_type_id = l.id WHERE p.contact_id = c.id AND p.is_primary LIMIT 1) AS phone_preffered,
    (SELECT e.email FROM civicrm_email AS e WHERE e.contact_id = c.id AND e.location_type_id = 1 LIMIT 1) AS email_home,
    (SELECT e.email FROM civicrm_email AS e WHERE e.contact_id = c.id AND e.location_type_id = 6 LIMIT 1) AS email_aktiv,
    (SELECT e.email FROM civicrm_email AS e WHERE e.contact_id = c.id AND e.location_type_id = 4 LIMIT 1) AS email_other,
    (SELECT e.email FROM civicrm_email AS e WHERE e.contact_id = c.id AND e.location_type_id = 2 LIMIT 1) AS email_work,
    "" as email_alternate,
    (SELECT l.name FROM civicrm_email AS e LEFT JOIN civicrm_location_type AS l ON e.location_type_id = l.id WHERE e.contact_id = c.id AND e.is_primary LIMIT 1) AS email_preffered,
    (SELECT contact_id_a FROM civicrm_relationship as r WHERE contact_id_b = c.id AND relationship_type_id = 5 LIMIT 1) as primary_contact,
    IF((SELECT count(*) FROM civicrm_group_contact AS gc WHERE gc.contact_id = c.id AND (gc.group_id = 1159 OR gc.group_id = 1061) AND status = "Added"), "TRUE", "FALSE") AS eoy_mailing_pdf
    FROM civicrm_contact as c
    LEFT JOIN civicrm_address as address ON c.id = address.contact_id
        LEFT JOIN civicrm_location_type as address_l ON address.location_type_id = address_l.id
        LEFT JOIN civicrm_state_province as address_sp ON address.state_province_id = address_sp.id
        LEFT JOIN civicrm_country as address_c ON address.country_id = address_c.id
    LEFT JOIN civicrm_value_weitere_daten_2 as w ON c.id = w.entity_id
    LEFT JOIN civicrm_value_aktivisten_21 as a ON c.id = a.entity_id
    LEFT JOIN civicrm_value_diverses_2_16 as d ON c.id = d.entity_id
    LEFT JOIN civicrm_value_wie_haben_sie_uns_kennen_gelernt__1 as k ON c.id = k.entity_id
    LEFT JOIN civicrm_value_mailchimp_felder_13 as m ON c.id = m.entity_id
    LEFT JOIN civicrm_website as web ON web.contact_id = c.id
        LEFT JOIN civicrm_option_value as o ON o.id = web.website_type_id AND option_group_id = 47
    LEFT JOIN civicrm_email as e ON c.id = e.contact_id
    WHERE c.is_deleted != 1 {where}
    GROUP BY c.id
    HAVING GROUP_CONCAT(e.email) NOT LIKE "%crm.animalequality.de%" OR GROUP_CONCAT(e.email) IS NULL';

$noteBaseQuery = 'SELECT
    n.entity_id as civi_contact_id,
    n.note,
    n.modified_date,
    n.subject
    FROM civicrm_note as n
    JOIN civicrm_contact as c ON c.id = n.entity_id
    WHERE n.entity_table = "civicrm_contact"' . specificContacts("n.entity_id", "AND") . ' {where}';

$donationReceiptBaseQuery = 'SELECT
    r.id as civi_id,
    "' . $importUser . '" as civi_sf_user,
    "' . $robinUser . '" as owner_id,
    status_5,
    issued_on_7,
    date_from_10,
    date_to_11,
    display_name_12,
    street_address_19,
    supplemental_address_1_20,
    postal_code_22,
    city_23,
    country_24,
    r.entity_id,
    IF(count(ri.issued_in_34) = 1, (SELECT receive_date FROM civicrm_contribution WHERE id = ri.entity_id), "") as receive_date,
    "per Post" as shipment_method,
    count(ri.issued_in_34) as number_items,
    REPLACE(sum(ri.total_amount_37), ".", ",") as sum_amount,
    "Original heruntergeladen" as status,
    CASE
        WHEN c.contact_type = "Organization" THEN "Organization"
        WHEN c.contact_type = "Individual" THEN "Kontakt"
    END as createtype
    FROM civicrm_value_donation_receipt_3 as r
    LEFT JOIN civicrm_value_donation_receipt_item_4 as ri ON r.id = ri.issued_in_34
    LEFT JOIN civicrm_contact as c ON c.id = r.entity_id WHERE status_5 = "ORIGINAL"' . specificContacts('r.entity_id', 'AND') . ' {where}
    GROUP BY r.id';

$contributionBaseQuery = 'SELECT
    c.id as civi_contribution_id,
    c.contact_id as civi_contact_id,
    "' . $importUser . '" as civi_sf_user,
    "' . $robinUser . '" as owner_id,
    CASE
        WHEN f.name = "allgemeine Spende" THEN "Donation"
        WHEN f.name = "Mitgliedsbeitrag" THEN "Membership"
        WHEN f.name = "Anlassspende" THEN "Donation"
        WHEN f.name = "Teilnahmegebühr" THEN "Donation"
        WHEN f.name = "Dauerspende Vereinsmeister" THEN "Membership"
        WHEN f.name = "Dauerspende" THEN "Membership"
        WHEN f.name = "Sponsoring" THEN "Donation"
        WHEN f.name = "Förderung" THEN "Grant"
    END as financial_type,
    CASE
        WHEN op.label = "Check" THEN "Transfer Check"
        WHEN op.label IN ("SEPA DD First Transaction", "SEPA DD Recurring Transaction", "SEPA DD One-off Transaction", "Lastschrift (Vereinsmeister)", "SEPA Einzeleinzug (Vereinsmeister)", "SEPA Dauereinzug (Vereinsmeister)", "SEPA-Lastschrift", "SEPA Automatische Abbuchung (Twingle)") THEN "Direct Debit"
        WHEN op.label = "Direktüberweisung" THEN "Direct Deposit"
        WHEN op.label = "Facebook donation" THEN "Facebook"
        WHEN op.label = "PayPal" THEN "PayPal REST API"
        ELSE op.label
    END as method,
    c.receive_date,
    REPLACE(c.total_amount, ".", ",") as total_amount,
    IF(financial_type_id = 7, "TRUE", "FALSE") as is_sponsoring,
    IF(financial_type_id = 7 OR c.non_deductible_amount != 0, "TRUE", "FALSE") as is_non_deductible,
    c.trxn_id,
    z.get_tigt_am_149,
    c.invoice_id,
    c.cancel_date,
    c.cancel_reason,
    c.thankyou_date,
    "Acknowledged" as acknowledged_status,
    c.source,
    oc.label as status,
    c.campaign_id,
    (SELECT issued_in_34 FROM civicrm_value_donation_receipt_item_4 JOIN civicrm_value_donation_receipt_3 ON civicrm_value_donation_receipt_3.id = civicrm_value_donation_receipt_item_4.issued_in_34 WHERE civicrm_value_donation_receipt_item_4.entity_id = c.id AND civicrm_value_donation_receipt_item_4.status_32 = "ORIGINAL") as receipt_id,
    GROUP_CONCAT(REPLACE(note,"\n", " ") SEPARATOR "; ") as notes
    FROM civicrm_contribution as c
    JOIN civicrm_contact as con ON con.id = c.contact_id
    LEFT JOIN civicrm_note as n ON c.id = n.entity_id AND entity_table = "civicrm_contribution"
    LEFT JOIN civicrm_financial_type as f ON c.financial_type_id = f.id
    LEFT JOIN civicrm_option_value as op ON c.payment_instrument_id = op.value AND op.option_group_id = 10
    LEFT JOIN civicrm_value_zusatzinforma_19 as z ON z.entity_id = c.id
    LEFT JOIN civicrm_option_value as oc ON c.contribution_status_id = oc.value AND oc.option_group_id = 11'
    . ' WHERE {where}'
    . fromDate('c.receive_date', 'AND')
    . specificContacts('c.contact_id', 'AND') .
    'GROUP BY c.id';

$contributionRecurringBaseQuery = 'SELECT
    c.id as civi_contact_id,
    rc.id as civi_contribution_recur_id,
    mem.id as civi_membership_id,
    IF(rc.id IS NOT NULL, REPLACE(rc.amount, ".", ","), REPLACE(bi.jahresbeitrag_73 / (12 / bi.zahlungsturnus_74) , ".", ",")) as amount,
    "' . $importUser . '" as civi_sf_user,
    "' . $robinUser . '" as owner_id,
    rc.cancel_date,
    CASE
        WHEN op.label = "Check" THEN "Transfer Check"
        WHEN op.label IN ("SEPA DD First Transaction", "SEPA DD Recurring Transaction", "SEPA DD One-off Transaction", "Lastschrift (Vereinsmeister)", "SEPA Einzeleinzug (Vereinsmeister)", "SEPA Dauereinzug (Vereinsmeister)", "SEPA-Lastschrift", "SEPA Automatische Abbuchung (Twingle)") THEN "Direct Debit"
        WHEN op.label = "Direktüberweisung" THEN "Direct Deposit"
        WHEN op.label = "Facebook donation" THEN "Facebook"
        WHEN op.label = "PayPal" THEN "PayPal REST API"
        ELSE op.label
    END as method,
    CASE
        WHEN mem.source IN ("per Telefon", "telefonisch", "Telefon", "Telefonisch", "Telefonisch bei Nora", "Telefonisch bei Matthias", "telefonisch bei Scarlett", "Telefonisch bei Jasmin", "Telefonisch bei Patrizia") THEN "Telefon"
        WHEN mem.source IN ("Mitgliedsformular", "Infostand", "per schriftlichem Formular", "Papierform") THEN "Mitgliedsformular (Papier)"
        WHEN mem.source IN ("Fundraisingbox", "Fundraisingbox (Spendenformular)", "Fundraisingbox (per Hand eingetragen") THEN "Fundraisingbox"
        WHEN mem.source IN ("E-Mail mit Spenderin", "E-Mailkontakt", "E-Mail mit Mitglied", "E-Mail Kontakt", "per E-Mail", "RLS", "E-Mail", "Email", "Zoom") THEN "E-Mail"
        WHEN mem.source IN ("Webform", "Mitgliedsformular (Online)", "Spendenformular", "Twingle") THEN "Webformular"
        WHEN mem.source IN ("Mitteilung Bank", "Post", "Brief", "Papierform") THEN "Post"
        WHEN mem.source IN ("Überweisung Verwendungszweck", "Absprache") THEN "Sonstiges"
        ELSE mem.source
    END AS original_source,
    CASE
        WHEN m.source IN ("per Telefon", "telefonisch", "Telefon", "Telefonisch", "Telefonisch bei Nora", "Telefonisch bei Matthias", "telefonisch bei Scarlett", "Telefonisch bei Jasmin", "Telefonisch bei Patrizia") THEN "Telefon"
        WHEN m.source IN ("Mitgliedsformular", "Infostand", "per schriftlichem Formular", "Papierform") THEN "Mitgliedsformular (Papier)"
        WHEN m.source IN ("Fundraisingbox", "Fundraisingbox (Spendenformular)", "Fundraisingbox (per Hand eingetragen") THEN "Fundraisingbox"
        WHEN m.source IN ("E-Mail mit Spenderin", "E-Mailkontakt", "E-Mail mit Mitglied", "E-Mail Kontakt", "per E-Mail", "RLS", "E-Mail", "Email", "Zoom") THEN "E-Mail"
        WHEN m.source IN ("Webform", "Mitgliedsformular (Online)", "Spendenformular", "Twingle") THEN "Webformular"
        WHEN m.source IN ("Mitteilung Bank", "Post", "Brief", "Papierform") THEN "Post"
        WHEN m.source IN ("Überweisung Verwendungszweck", "Absprache") THEN "Sonstiges"
        ELSE m.source
    END AS current_source,
    mem.join_date,
    IF(rc.id IS NOT NULL, IF(rc.frequency_unit = "month", "Monthly", IF(rc.frequency_unit = "year", "Yearly", rc.frequency_unit)), "monthly") as frequency_unit,
    DATE_SUB(rc.next_sched_contribution_date, INTERVAL 2 DAY) as next_sched_contribution_date,
    IF(rc.id IS NOT NULL, rc.frequency_interval, bi.zahlungsturnus_74) as frequency_interval,
    rc.create_date,
    mem.end_date,
    DATE_SUB(rc.next_sched_contribution_date, INTERVAL 2 DAY) AS start_date,
    rc.trxn_id,
    mem.start_date as member_start_date,
    bm.k_ndigungsdatum_111,
    IF(bi.wird_per_dauerauftrag_bezahlt_80 = 0 OR bi.wird_per_dauerauftrag_bezahlt_80 IS NULL, "FALSE", "TRUE") as wird_per_dauerauftrag_bezahlt_80,
    IF(rc.cycle_day = 3, 1, IF(rc.cycle_day = 15, 13, rc.cycle_day)) as cycle_day,
    mem.campaign_id,
    (SELECT GROUP_CONCAT(note SEPARATOR "\n\n") FROM civicrm_note WHERE civicrm_note.entity_id = rc.id AND entity_table = "civicrm_contribution_recur" GROUP BY rc.id) as notes,
    CASE
        WHEN ms.name = "New" THEN "Active"
        WHEN ms.name = "Current" THEN "Active"
        WHEN ms.name = "Expired" THEN "Closed"
        WHEN ms.name = "Cancelled" THEN "Closed"
        WHEN ms.name = "Pending" THEN "Pending"
        WHEN ms.name = "Deceased" THEN "Deceased"
        WHEN ms.name = "Gestrichen" THEN "Annulled"
        WHEN ms.name = "Pausiert" THEN "Paused"
        ELSE ms.name
    END AS status,
    m.iban,
    m.bic,
    m.reference as mandatsreferenz,
    (SELECT contact_id_a FROM civicrm_relationship as r WHERE contact_id_b = c.id AND relationship_type_id = 5 LIMIT 1) as primary_contact
    FROM civicrm_contact AS c
    LEFT JOIN civicrm_contribution_recur as rc ON rc.contact_id = c.id
    LEFT JOIN civicrm_membership as mem ON c.id = mem.contact_id
    LEFT JOIN civicrm_option_value AS op ON rc.payment_instrument_id = op.value AND op.option_group_id = 10
    LEFT JOIN civicrm_membership_status AS ms ON mem.status_id = ms.id
    LEFT JOIN civicrm_sdd_mandate AS m ON rc.id = m.entity_id AND m.entity_table = "civicrm_contribution_recur"
    LEFT JOIN civicrm_value_benutzerdefinierte_felder_zu_mitgliedschaf_6 as bm ON bm.entity_id = mem.id
    LEFT JOIN civicrm_value_beitragsinformationen_7 as bi ON bi.entity_id = mem.id
    WHERE {where}'
    . specificContacts('c.id', 'AND');

$contributionRecurringNonActiveBaseQuery = 'SELECT
    c.id as civi_contact_id,
    "" as civi_contribution_recur_id,
    mem.id as civi_membership_id,
    REPLACE(bi.jahresbeitrag_73 / (12 / bi.zahlungsturnus_74) , ".", ",") as amount,
    "' . $importUser . '" as civi_sf_user,
    "' . $robinUser . '" as owner_id,
    "" as cancel_date,
    "" as method,
    CASE
        WHEN mem.source IN ("per Telefon", "telefonisch", "Telefon", "Telefonisch", "Telefonisch bei Nora", "Telefonisch bei Matthias", "telefonisch bei Scarlett", "Telefonisch bei Jasmin", "Telefonisch bei Patrizia") THEN "Telefon"
        WHEN mem.source IN ("Mitgliedsformular", "Infostand", "per schriftlichem Formular", "Papierform") THEN "Mitgliedsformular (Papier)"
        WHEN mem.source IN ("Fundraisingbox", "Fundraisingbox (Spendenformular)", "Fundraisingbox (per Hand eingetragen") THEN "Fundraisingbox"
        WHEN mem.source IN ("E-Mail mit Spenderin", "E-Mailkontakt", "E-Mail mit Mitglied", "E-Mail Kontakt", "per E-Mail", "RLS", "E-Mail", "Email", "Zoom") THEN "E-Mail"
        WHEN mem.source IN ("Webform", "Mitgliedsformular (Online)", "Spendenformular", "Twingle") THEN "Webformular"
        WHEN mem.source IN ("Mitteilung Bank", "Post", "Brief", "Papierform") THEN "Post"
        WHEN mem.source IN ("Überweisung Verwendungszweck", "Absprache") THEN "Sonstiges"
        ELSE mem.source
    END AS original_source,
    "" as current_source,
    mem.join_date,
    "" as frequency_unit,
    "" as next_sched_contribution_date,
    "" as frequency_interval,
    "" as create_date,
    mem.end_date,
    "" as start_date,
    "" as trxn_id,
    mem.start_date as member_start_date,
    bm.k_ndigungsdatum_111,
    IF(bi.wird_per_dauerauftrag_bezahlt_80 = 0 OR bi.wird_per_dauerauftrag_bezahlt_80 IS NULL, "FALSE", "TRUE") as wird_per_dauerauftrag_bezahlt_80,
    "" as cycle_day,
    mem.campaign_id,
    "" as notes,
    CASE
        WHEN ms.name = "New" THEN "Active"
        WHEN ms.name = "Current" THEN "Active"
        WHEN ms.name = "Expired" THEN "Closed"
        WHEN ms.name = "Cancelled" THEN "Closed"
        WHEN ms.name = "Pending" THEN "Pending"
        WHEN ms.name = "Deceased" THEN "Deceased"
        WHEN ms.name = "Gestrichen" THEN "Annulled"
        WHEN ms.name = "Pausiert" THEN "Paused"
        ELSE ms.name
    END AS status,
    "" as iban,
    "" as bic,
    "" as mandatsreferenz,
    (SELECT contact_id_a FROM civicrm_relationship as r WHERE contact_id_b = c.id AND relationship_type_id = 5 LIMIT 1) as primary_contact
    FROM civicrm_membership AS mem
    LEFT JOIN civicrm_contact as c ON c.id = mem.contact_id
    LEFT JOIN civicrm_membership_status AS ms ON mem.status_id = ms.id
    LEFT JOIN civicrm_value_benutzerdefinierte_felder_zu_mitgliedschaf_6 as bm ON bm.entity_id = mem.id
    LEFT JOIN civicrm_value_beitragsinformationen_7 as bi ON bi.entity_id = mem.id
    WHERE mem.status_id NOT IN (1, 2) {where}'
    . specificContacts('c.id', 'AND');

$campaignMemberGroupBaseQuery = 'SELECT
    g.group_id as civi_campaign_id,
    g.contact_id as civi_entity_id,
    "Confirmed" as status,
    "' . $importUser . '" as civi_sf_user,
    (SELECT MAX(sh.date) FROM civicrm_subscription_history as sh WHERE sh.contact_id = g.contact_id AND sh.group_id = g.group_id AND sh.status = "Added") as created_date,
    "group" as type
    FROM civicrm_group_contact as g
    JOIN civicrm_contact as c ON c.id = g.contact_id
    WHERE {where} AND status = "Added" AND group_id IN (1301, 1320, 1323, 1360, 1363, 1364, 1365, 1366, 1367, 1368, 947)'  . specificContacts('g.contact_id', 'AND') . fromDate('created_date', 'AND');

$campaignMemberPetitionBaseQuery = 'SELECT
    g.group_id as civi_campaign_id,
    g.contact_id as civi_entity_id,
    "Confirmed" as status,
    "' . $importUser . '" as civi_sf_user,
    "Petition" as source,
    (SELECT street_address FROM civicrm_address as a WHERE a.contact_id = g.contact_id LIMIT 1) as street,
    (SELECT city FROM civicrm_address as a WHERE a.contact_id = g.contact_id LIMIT 1) as city,
    (SELECT postal_code FROM civicrm_address as a WHERE a.contact_id = g.contact_id LIMIT 1) as postcode,
    (SELECT c.iso_code FROM civicrm_address as a JOIN civicrm_country as c ON c.id = a.country_id WHERE a.contact_id = g.contact_id LIMIT 1) as country,
    (SELECT MAX(sh.date) FROM civicrm_subscription_history as sh WHERE sh.contact_id = g.contact_id AND sh.group_id = g.group_id AND sh.status = "Added") as from_date,
    "petition" as type
    FROM civicrm_group_contact as g
    JOIN civicrm_contact as c ON c.id = g.contact_id
    WHERE {where} AND status = "Added" AND group_id IN (' . implode(',', $petitionGroupsIds) . ')'  . specificContacts('g.contact_id', 'AND') . ' HAVING from_date > "2022-05-04 10:40:00"';

$campaignMemberTagBaseQuery = 'SELECT
    t.tag_id as civi_campaign_id,
    t.entity_id as civi_entity_id,
    "Confirmed" as status,
    "' . $importUser . '" as civi_sf_user,
    "tag" as type
    FROM civicrm_entity_tag as t
    JOIN civicrm_contact as c ON c.id = t.entity_id
    WHERE {where} AND t.entity_table = "civicrm_contact" AND tag_id IN (' . implode(',', $tagIdsToExport) . ')' . specificContacts('t.entity_id', 'AND');

$queries = [
    [
        'name' => 'contact',
        'query' => str_replace('{where}', 'AND contact_type = "Individual" ' . $contactDefiningCriteria . specificContacts('c.id', 'AND') . fromDate('c.created_date', 'AND'), $contactBaseQuery)
    ],
    [
        'name' => 'lead',
        'query' => str_replace('{where}', 'AND contact_type = "Individual" '. $leadDefiningCriteria . specificContacts('c.id', 'AND') . fromDate('c.created_date', 'AND'), $contactBaseQuery)
    ],
    [
        'name' => 'organization',
        'query' => str_replace('{where}', 'AND contact_type = "Organization" ' . specificContacts('c.id', 'AND') . fromDate('c.created_date', 'AND'), $contactBaseQuery)
    ],
    [
        'name' => 'organization_primary_contacts_without_donation',
        'query' => str_replace('{where}', 'AND contact_type = "Individual" AND (SELECT contact_id_b FROM civicrm_relationship as r WHERE contact_id_a = c.id AND relationship_type_id = 5 LIMIT 1) IS NOT NULL AND c.id NOT IN (' . $contactIdsContribution . ')' . specificContacts('c.id', 'AND'), $contactBaseQuery)
    ],
    [
        'name' => 'organization_to_primary_contacts_without_donation',
        'query' => 'SELECT (SELECT contact_id_b FROM civicrm_relationship as r WHERE contact_id_a = c.id AND relationship_type_id = 5 LIMIT 1) AS organization, c.id as primary_contact FROM civicrm_contact as c  WHERE contact_type = "Individual" AND (SELECT contact_id_b FROM civicrm_relationship as r WHERE contact_id_a = c.id AND relationship_type_id = 5 LIMIT 1) IS NOT NULL AND c.id NOT IN (' . $contactIdsContribution . ')' . specificContacts('c.id', 'AND')
    ],
    [
        'name' => 'contribution_contact',
        'query' => str_replace('{where}', 'con.contact_type = "Individual" AND c.financial_type_id = 1 AND c.payment_instrument_id != 7', $contributionBaseQuery)
    ],
    [
        'name' => 'contribution_organization',
        'query' => str_replace('{where}', 'con.contact_type = "Organization" AND c.financial_type_id = 1 AND c.payment_instrument_id != 7', $contributionBaseQuery)
    ],
    [
        'name' => 'contribution_recurring_contact_active_sepa',
        'query' => str_replace('{where}', 'c.contact_type = "Individual" AND rc.contribution_status_id = 2 AND mem.status_id IN (1, 2)' . fromDate('rc.create_date', 'AND'), $contributionRecurringBaseQuery)
    ],
    [
        'name' => 'contribution_recurring_contact_active_non_sepa',
        'query' => str_replace('{where}', 'c.contact_type = "Individual" AND (mem.status_id IN (1, 2)) AND rc.id IS NULL' . fromDate('mem.join_date', 'AND'), $contributionRecurringBaseQuery)
    ],
    [
        'name' => 'contribution_recurring_contact_non_active',
        'query' => str_replace('{where}', 'AND c.contact_type = "Individual"', $contributionRecurringNonActiveBaseQuery)
    ],
    [
        'name' => 'contribution_recurring_organization_active_sepa',
        'query' => str_replace('{where}', 'c.contact_type = "Organization" AND rc.contribution_status_id = 2 AND mem.status_id IN (1, 2)' . fromDate('rc.create_date', 'AND'), $contributionRecurringBaseQuery)
    ],
    [
        'name' => 'contribution_recurring_organization_active_non_sepa',
        'query' => str_replace('{where}', 'c.contact_type = "Organization" AND (mem.status_id IN (1, 2)) AND rc.id IS NULL', $contributionRecurringBaseQuery) . fromDate('mem.join_date', 'AND')
    ],
    [
        'name' => 'contribution_recurring_organization_non_active',
        'query' => str_replace('{where}', 'AND c.contact_type = "Organization"', $contributionRecurringNonActiveBaseQuery)
    ],
    [
        'name' => 'donation_receipt_contact',
        'query' => str_replace('{where}', 'AND c.contact_type = "Individual" ', $donationReceiptBaseQuery)
    ],
    [
        'name' => 'donation_receipt_organization',
        'query' => str_replace('{where}', 'AND c.contact_type = "Organization" ', $donationReceiptBaseQuery)
    ],
    [
        'name' => 'pardot_newsletter',
        'query' => 'SELECT
            c.id,
            (SELECT e.email FROM civicrm_email AS e WHERE e.contact_id = c.id AND e.is_primary LIMIT 1) AS email
            FROM civicrm_contact as c
            LEFT JOIN civicrm_email as e ON c.id = e.contact_id
            WHERE c.is_deleted != 1 AND contact_type = "Individual" AND (SELECT count(*) FROM civicrm_group_contact AS gc WHERE gc.contact_id = c.id AND gc.group_id = 676 AND status = "Added") >= 1' . specificContacts('c.id', 'AND') . '
            GROUP BY c.id
            HAVING GROUP_CONCAT(e.email) NOT LIKE "%crm.animalequality.de%" OR GROUP_CONCAT(e.email) IS NULL'
    ],
    [
        'name' => 'pardot_one_time_donors',
        'query' => 'SELECT
            c.id,
            (SELECT e.email FROM civicrm_email AS e WHERE e.contact_id = c.id AND e.is_primary LIMIT 1) AS email
            FROM civicrm_contact as c
            LEFT JOIN civicrm_email as e ON c.id = e.contact_id
            LEFT JOIN civicrm_contribution as con ON c.id = con.contact_id
            LEFT JOIN civicrm_contribution_recur as rec ON c.id = rec.contact_id
            LEFT JOIN civicrm_membership as mem ON c.id = mem.contact_id
            WHERE c.is_deleted != 1 AND
                contact_type = "Individual"' . specificContacts('c.id', 'AND') . ' AND
                con.id IS NOT NULL AND
                rec.id IS NULL AND
                mem.id IS NULL
            GROUP BY c.id
            HAVING GROUP_CONCAT(e.email) NOT LIKE "%crm.animalequality.de%" OR GROUP_CONCAT(e.email) IS NULL'
    ],
    [
        'name' => 'pardot_recurring_donors',
        'query' => 'SELECT
            c.id,
            (SELECT e.email FROM civicrm_email AS e WHERE e.contact_id = c.id AND e.is_primary LIMIT 1) AS email
            FROM civicrm_contact as c
            LEFT JOIN civicrm_email as e ON c.id = e.contact_id
            LEFT JOIN civicrm_contribution_recur as rec ON c.id = rec.contact_id
            LEFT JOIN civicrm_membership as mem ON c.id = mem.contact_id
            WHERE c.is_deleted != 1 AND
                contact_type = "Individual"' . specificContacts('c.id', 'AND') . ' AND
                rec.id IS NOT NULL OR
                mem.id IS NOT NULL
            GROUP BY c.id
            HAVING GROUP_CONCAT(e.email) NOT LIKE "%crm.animalequality.de%" OR GROUP_CONCAT(e.email) IS NULL'
    ],
    [
        'name' => 'campaign',
        'query' => 'SELECT
            c.id as civi_campaign_id,
            c.title,
            c.description,
            c.start_date,
            c.end_date,
            os.name as status,
            IF(os.name IS NULL, "Completed", os.name) as status,
            IF(c.is_active = 0, "FALSE", "TRUE") as is_active,
            c.created_date,
            "' . $importUser . '" as civi_sf_user,
            "' . $robinUser . '" as owner_id,
            c.goal_revenue,
            CASE
                WHEN o.label = "Auszeichnung" THEN "Event"
                WHEN o.label = "Constituent Engagement" THEN "General"
                WHEN o.label = "Direct Mail" THEN "Marketing"
                WHEN o.label = "Outreach Campaign" THEN "General"
                WHEN o.label = "Political Campaign" THEN "General"
                WHEN o.label = "Verdoppelungsaktion" THEN "Fundraising"
            END as record_type,
            CASE
                WHEN o.label = "Auszeichnung" THEN "Social Event"
                WHEN o.label = "Constituent Engagement" THEN "Public Relations"
                WHEN o.label = "Direct Mail" THEN "Direct Mail"
                WHEN o.label = "Outreach Campaign" THEN "Public Relations"
                WHEN o.label = "Political Campaign" THEN "Public Relations"
                WHEN o.label = "Verdoppelungsaktion" THEN "Matching Gift"
            END as type
        FROM civicrm_campaign as c
        LEFT JOIN civicrm_option_value as o ON c.campaign_type_id = o.value AND o.option_group_id = 52
        LEFT JOIN civicrm_option_value as os ON c.status_id = os.value AND os.option_group_id = 53
        WHERE c.id IN (' . implode(',', $campaignIdsToExport) . ')'
    ],
    [
        'name' => 'campaign_member_groups_contact',
        'query' => str_replace('{where}', 'c.contact_type = "Individual"' . $contactDefiningCriteria, $campaignMemberGroupBaseQuery)
    ],
    [
        'name' => 'campaign_member_petitions_contact',
        'query' => str_replace('{where}', 'c.contact_type = "Individual"' . $contactDefiningCriteria, $campaignMemberPetitionBaseQuery)
    ],
    [
        'name' => 'campaign_member_tags_contact',
        'query' => str_replace('{where}', 'c.contact_type = "Individual"' . $contactDefiningCriteria, $campaignMemberTagBaseQuery)
    ],
    [
        'name' => 'campaign_member_groups_lead',
        'query' => str_replace('{where}', 'c.contact_type = "Individual"' . $leadDefiningCriteria, $campaignMemberGroupBaseQuery)
    ],
    [
        'name' => 'campaign_member_petitions_lead',
        'query' => str_replace('{where}', 'c.contact_type = "Individual"' . $leadDefiningCriteria, $campaignMemberPetitionBaseQuery)
    ],
    [
        'name' => 'campaign_member_tags_lead',
        'query' => str_replace('{where}', 'c.contact_type = "Individual"' . $leadDefiningCriteria, $campaignMemberTagBaseQuery)
    ],
    [
        'name' => 'campaign_member_groups_organization',
        'query' => str_replace('{where}', 'c.contact_type = "Organization"', $campaignMemberGroupBaseQuery)
    ],
    [
        'name' => 'campaign_member_petitions_organization',
        'query' => str_replace('{where}', 'c.contact_type = "Organization"', $campaignMemberPetitionBaseQuery)
    ],
    [
        'name' => 'campaign_member_tags_organization',
        'query' => str_replace('{where}', 'c.contact_type = "Organization"', $campaignMemberTagBaseQuery)
    ],
    [
        'name' => 'mailing_openings',
        'query' => 'SELECT
            c.id,
            (SELECT count(DISTINCT mailing_id) FROM civicrm_mailing_event_opened AS o JOIN civicrm_mailing_event_queue as q ON o.event_queue_id = q.id JOIN civicrm_mailing_job as j ON q.job_id = j.id WHERE q.contact_id = c.id GROUP BY q.contact_id) AS openings,
            (SELECT email from civicrm_email as e WHERE e.is_primary = 1 AND e.contact_id = c.id LIMIT 1) as email
            FROM civicrm_contact as c
            WHERE c.contact_type = "Individual" AND c.id IN (SELECT gc.contact_id FROM civicrm_group_contact AS gc WHERE gc.status = "Added" AND group_id IN(676))
            ORDER BY openings DESC'
    ],
    // [
    //     'name' => 'note_contact',
    //     'query' => str_replace('{where}', 'AND c.contact_type = "Individual" AND n.entity_id IN (' . $contactIdsContribution . ')', $noteBaseQuery)
    // ],
    // [
    //     'name' => 'note_lead',
    //     'query' => str_replace('{where}', 'AND n.entity_id NOT IN (' . $contactIdsContribution . ') AND n.entity_id IN (' . $contactIdsInNewsletterOrPetitionGroup . ')', $noteBaseQuery)
    // ],
    // [
    //     'name' => 'note_organization',
    //     'query' => str_replace('{where}', 'AND c.contact_type = "Organization"', $noteBaseQuery)
    // ]
];

foreach ($queries as $query) {
    //var_dump($query);
    //continue;

    if (strpos($query['name'], 'petition') === false) {
        //continue;
    }

    try {
        $firstRow = true;
        $fp = fopen('exports/' . $query['name'] . '.csv', 'w');
        $statement = $pdo->query($query['query']);

        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            if ($firstRow) {
                $headers = array_keys($row);
                fputcsv($fp, $headers, $separator);
                $firstRow = false;
            }

            fputcsv($fp, alterRow($query['name'], $row), $separator);
        }

        if ($query['name'] === 'campaign') {
            addPetitionCampaigns($fp);
        }

        fclose($fp);
    } catch(Exception $e) {
        var_dump($e);
    }
}

exportCampaignMembers('lead');
exportCampaignMembers('contact');
exportCampaignMembers('organization');